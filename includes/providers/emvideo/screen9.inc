<?php

/**
 * @file
 * media_screen9.inc
 *
 * Provide support for the screen9 provider to the emfield.module.
 */

/**
 * hook emvideo_PROVIDER_info
 * this returns information relevant to a specific 3rd party video provider
 * @return array
 *   an array of strings requested by various admin and other forms
 *   'name' => the translated name of the provider
 *   'url' => the url to the main page for the provider
 *   'settings_description' => a description of the provider that will
 *      be posted in the admin settings form
 *   'supported_features' => an array of rows describing the state
 *      of certain supported features by the provider.
 *   These will be rendered in a table, with the columns being
 *      'Feature', 'Supported', 'Notes'.
 */
function screen9_emvideo_screen9_info() {
  $features = array(
    array(
      t('Duration'),
      t('Yes'),
      t("A video's duration can be displayed using view formatters.'"),
    ),
  );
  return array(
    'provider' => 'screen9',
    'name' => t('screen9'),
    'url' => 'http://www.picsearch.com',
    'settings_description' => t('These settings specifically affect videos displayed from <a href="http://www.picsearch.com" target="_blank">screen9</a>.'),
    'supported_features' => $features,
  );
}

/**
 * hook emvideo_PROVIDER_extract
 * this is called to extract the video code from a pasted URL or embed code.
 *
 * @param string $video_code
 *   an optional string with the pasted URL or embed code
 *
 * @return array
 *   either an array of regex expressions to be tested, or a string with the
 *   video code to be used.
 *   if the hook tests the code itself, it should return either the string of
 *   the video code (if matched), or an empty array.
 *   otherwise, the calling function will handle testing the embed code
 *   against each regex string in the returned array.
 */
function screen9_emvideo_screen9_extract($video_code = '') {

  $id = screen9_string2mediaid($video_code);
  if ($id === FALSE) {
      return array();
  }
  return $id;
}

/**
 * hook emvideo_PROVIDER_embedded_link($video_code)
 * returns a link to view the video at the provider's site
 *
 * @param string $video_code
 *   the string containing the video to watch
 *
 * @return array
 *   a string containing the URL to view the video at the
 *   original provider's site
 */
function screen9_emvideo_screen9_embedded_link($video_code) {
  return screen9_call(
    'getMediaContentLink',
    array($video_code, variable_get('screen9_setting_lifetime', 0), "", 0),
  );
}

/**
 * hook emvideo_PROVIDER_video
 * this actually displays the full/normal-sized video we want,
 * usually on the default page view
 *
 * @param string $video_code
 *   the video code for the video to embed
 * @param float $width
 *   the width to display the video
 * @param float $height
 *   the height to display the video
 * @param string $field
 *   the field info from the requesting node
 * @param string $item
 *   the actual content from the field
 *
 * @return array
 *   the html of the embedded video
 */
function screen9_emvideo_screen9_video($video_code, $width, $height, $field, $item, $node, $autoplay, $options = array()) {
  $options['width'] = isset($options['width']) ? $options['width'] : $width;
  $options['height'] = isset($options['height']) ? $options['height'] : $height;

  $embed = screen9_call('getPresentation', array($video_code, variable_get('screen9_setting_lifetime', 0), array(
    'embedtype'    => 'playertag',
    'playerheight' => $options['height'],
    'playerwidth'  => $options['width'],
  ),),
  );
  return $embed['playertag'];
}

/**
 * hook emvideo_PROVIDER_video
 * this actually displays the preview-sized video we want,
 * commonly for the teaser
 *
 * @param string $video_code
 *   the video code for the video to embed
 * @param float $width
 *   the width to display the video
 * @param float $height
 *   the height to display the video
 * @param string $field
 *   the field info from the requesting node
 * @param string $item
 *   the actual content from the field
 *
 * @return string
 *   the html of the embedded video
 */
function screen9_emvideo_screen9_preview($video_code, $width, $height, $field, $item, $node, $autoplay, $options = array()) {
  $options['width'] = isset($options['width']) ? $options['width'] : $width;
  $options['height'] = isset($options['height']) ? $options['height'] : $height;

  $embed = screen9_call('getPresentation', array($video_code, variable_get('screen9_setting_lifetime', 0), array(
    'embedtype' => 'playertag',
    'playerheight' => $options['height'],
    'playerwidth' => $options['width'],
  ),));
  return $embed['playertag'];
}


/**
 * hook emvideo_PROVIDER_thumbnail
 * returns the external url for a thumbnail of a specific video
 *
 * @param string $field
 *   the field of the requesting node
 * @param string $item
 *   the actual content of the field from the requesting node
 *
 * @return string
 *   a URL pointing to the thumbnail
 */
function screen9_emvideo_screen9_thumbnail($field, $item, $formatter, $node, $width, $height) {
  $fields = array(
    'title',
    'categoryid',
    'categoryname',
    'mediaid',
    'processing_progress',
    'properties',
    'status',
    'username',
    'thumbnail',
  );
  $media = screen9_call('getMediaDetails', array($item['value'], $fields));
  return $media['thumbnail'];
}

/**
 * Implements hook_emvideo_PROVIDER_content_generate().
 */
function screen9_emvideo_screen9_content_generate() {

  return array();
}

/**
 * Implments hook emvideo_PROVIDER_data_version().
 */
function screen9_emvideo_screen9_data_version() {
  return 1;
}

/**
 * Implments hook_emvideo_PROVIDER_data().
 */
function screen9_emvideo_screen9_data($field, $item) {

  $fields = array(
    'title',
    'categoryid',
    'categoryname',
    'mediaid',
    'processing_progress',
    'properties',
    'status',
    'username',
    'thumbnail',
  );
  $media = screen9_call('getMediaDetails', array($item['value'], $fields));
  return array(
    'title' => $media['title'],
    'categoryname' => $media['categoryname'],
    'status' => $media['status'],
  );
}

/**
 * hook emvideo_PROVIDER_duration($item)
 * Returns the duration of the video in seconds.
 *
 * @param float $item
 *   The video item itself, which needs the $data array.
 *
 * @return float
 *   The duration of the video in seconds.
 */
function screen9_emvideo_screen9_duration($item) {
  $fields = array(
    'title',
    'categoryid',
    'categoryname',
    'mediaid',
    'processing_progress',
    'properties',
    'status',
    'username',
    'thumbnail',
    'duration',
  );
  $media = screen9_call('getMediaDetails', array($item['value'], $fields));
  return $media['duration'];
}
