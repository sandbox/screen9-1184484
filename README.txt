Screen9 Online Video Platform
Drupal usage guide
1 June 2011

Contents

  1  Introduction
  2  Configuration
  3  Uploading video
  4  Publishing video
  5  Editing the published video


1  Introduction
---------------

This is a usage guide to get started using the Screen9 OVP module for Drupal.
The reader is assumed to have:

  *  Basic understanding of how to use Drupal.
  *  Access to a Drupal installation where the Screen9 Drupal module is
     installed.
  *  Login access to the Screen9 Account Console.

The Screen9 Drupal module makes uploading and publishing of videos in your
Drupal site easy  but the module only implements a subset of the features and
functions available in the Screen9 platform. To access additional features, you
may need to log in to your Screen9 Account Console and edit your videos there.
Any changes you make to content there will be automatically reflected in your
Drupal video content and vice versa.


2  Configuration
----------------

Your Screen9 Drupal module should already be set up according to the Screen9
OVP Drupal  setup guide. To verify this, navigate to
Site building->modules->list and verify that the  modules "Media: screen9",
"screen9 api", "screen9 cck" and "screen9 wysiwyg" are in your list and
checked.

When you have verified that the modules are checked you should be able to see
the videos  in your Screen9 account by browsing to My account and selecting
Videos. The videos in your account will show up in a list with thumbnail,
Title, Status, Category and a column to see details or delete a video. The
Videos view also has a link to upload videos to your account.

If this is not the case, contact your system administrator for verification
that the installation is properly configured.


3  Uploading video
------------------

Videos already in your account or that have been uploaded via for instance the
Screen9 account console will automatically appear in your list of videos. If
you wish to upload new content to your account, you can do so using the Drupal
interface. This can be done in three different ways:

  1.  Navigate to My account->Videos and select the Upload button.
  2.  Navigate to Create content and select Screen9 video.
  3.  You can also choose to upload a video in the dialog box where you select
      a video for embedding but the recommended way is to use option 1 or 2.

To upload your video, simply click Choose file and select the video from the
file browser. 
Optionally set the category, title and description (this is recommended but not
required) for your video and click the Upload button.


4  Publishing video
-------------------

Publishing videos using the TinyMCE WYSIWYG editor is very simple but please
note that embedding of video will only work in "Full HTML" input mode so make
sure that is what you are using.

Simply click the Screen9 video icon and a dialog popup to select your video
will appear. Here you can choose the video that you wish to embed. To locate
your video, you can browse the account using the bottom navigation or search
for your video using the top right search box. Click Select to choose the video
you want to embed.

The video will be embedded in the default width that you have configured for
your Drupal account (this can be changed under Site configuration->Screen9
settings). The height of the video will be automatically adjusted to fit the
aspect ratio of the video.

The Screen9 module also comes with a CCK field. To use the CCK field, click
manage fields for the desired content type and add the content field “screen9
Video”. When this is done, videos can be published as CCK types in pages. With
the CCK types, width and height for your video embed must be filled out. If the
Maintain aspect ratio box is checked, the height of the player will be
automatically adjusted to fit the video.


5 Editing the published video
-----------------------------

Videos embedded using the TinyMCE editor can be edited for size and position.
Simply click the Screen9 video icon and the edit video dialog will appear.

The edit video dialog displays the current title and description for the video
as well as the thumbnail. Using the top right Change video button you can
select another video if you wish to replace the video in the same position.

The size of the video is changed by editing the Dimensions box. If the Maintain
aspect ratio box is ticked, the width and height will automatically update to
fit the aspect ratio of the video if the height or width is updated
respectively.

Alignment of the embedded video can be controlled by choosing from the Align
dropdown box.


Copyright © 2011 Screen9 AB
screen9.com
Liljeholmsvägen 30
SE 117 61 Stockholm, SWEDEN
+46 (0)8 641 22 30
licensing@screen9.com
