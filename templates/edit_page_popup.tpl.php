<?php

/**
 * @file
 * edit_page_popup.tpl.php
 *
 * Template for edit video popup.
 */

?>

<html>
  <head>
    <link href='/themes/garland/style.css' media='all' rel='stylesheet' type='text/css' />
    <script type="text/javascript" src="http://jqueryjs.googlecode.com/files/jquery-1.3.2.min.js"></script>
    <script type="text/javascript"><?php echo $op_script ?></script>
  </head>
<body style='padding: 20px'>
  <div>
    <h2><?php echo t("Edit video") ?></h2>
    <div style='float:left;margin-top:15px'><p><span style='font-size:10px'>Video title</span><br /><?php echo $title ?></p></div><div style='float:right;margin-top:10px'><button onclick="window.open('/screen9/selectpopup/?t=<?php echo $mid ?>', 'screen9', 'width=600, scrollbars=1');return false;" href="<?php echo url('screen9/selectpopup') ?>"> Change video </button></div>
      <br clear='all' /><p><span style='font-size:10px'>Video description</span><br /><?php echo $description  ?></p>
      <img width='250' src='<?php echo $details['thumbnail'] ?>' alt='thumbnail' />
      <br /><br /><hr />
        <?php echo $utable ?>
      <hr /><br />
      <div style='float:right'><input type='button' value='Cancel' onclick='window.close()' /> <input id='update' type='button' value='Update' /></div>
  </div>
  </body>
</html>
