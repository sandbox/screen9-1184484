<?php

/**
 * @file
 * select_page_popup.tpl.php
 *
 * Template for Select video popup.
 */

?>

<html>
  <head>
    <meta http-equiv='refresh' content='15'>
    <link href='/modules/system/system.css' media='all' rel='stylesheet' type='text/css' />
    <link href='/themes/garland/style.css' media='all' rel='stylesheet' type='text/css' />
    <script type="text/javascript" src="http://jqueryjs.googlecode.com/files/jquery-1.3.2.min.js"></script>
    <script type="text/javascript">
      <?php echo $op_script ?>
    </script>
  </head>
  <body style='padding: 20px'>
  <?php if ($upload): ?>
    <h2><?php echo t("Upload video") ?></h2><br />
    Go&nbsp;to&nbsp;<a onclick="window.open('<?php echo url('user/' . $user->uid . '/screen9/upload',
      array(
        'query' => array(
          'returnmediaid' => 'true', 'redir' => 'true', 't' => $t))) ?>
          ', 'screen9', 'width=450, height=400, left=\'+((screen.width - 320)/2)+\',
          top=\'+((screen.height - 500)/2)+\'');return false;" href="
          <?php url('user/' . $user->uid . '/screen9/upload', array(
        'query' =>
      array('returnmediaid' => 'true', 'redir' => 'true'))) ?>
        ">upload</a> page.<br /><br />
  <?php endif; ?>

  <form method="GET" action="">
  <input type="hidden" name="q" value="<?php echo $_GET['q'] ?>"/>
  <input type="hidden" name="t" value="<?php echo $t ?>"/>

  <?php if ($a == 1): ?>
    <input type=\"hidden\" name=\"a\" value=\"1\"/>
  <?php endif; ?>

  <div style="float:right">
    <input type="text" name="searchtext" value="<?php echo $search ?>"/>
    <input type="submit" value="Search"/>
  </div>

  <h2>Choose existing video</h2><br />
  <?php echo $table; ?>
  <?php echo theme('pager', NULL, 5, 0); ?>
  <?php if ($search != ''): ?>
    <br />
      <form>
        <input type='button' value="<?php echo t('Go back') ?>" onclick='history.go(-1)' />
      </form>
  <?php endif; ?>
  </form>
  </body>
</html>
