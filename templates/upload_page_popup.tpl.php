<?php

/**
 * @file
 * upload_page_popup.tpl.php
 *
 * Template for upload video popup.
 */

?>

<html>
  <head>
    <link href='/themes/garland/style.css' media='all' rel='stylesheet' type='text/css' />
    <link href='/modules/system/system.css' media='all' rel='stylesheet' type='text/css' />
    <script type="text/javascript" src="http://jqueryjs.googlecode.com/files/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="/<?php echo 'misc/jquery.form.js' ?>"></script>
    <script type="text/javascript" src="/<?php echo drupal_get_path('module', 'screen9') . '/js/screen9.js' ?>"></script>
  </head>
<body style='padding: 20px;'>
  <h2><?php echo t("Upload media file") ?></h2>
    <form accept-charset="UTF-8" method="post" id="screen9upload" enctype="multipart/form-data" action="<?php echo $uploadurl['url'] ?>?uploadid=<?php echo $uploadurl['mediaid'] ?>">

    <table>
      <tbody style='border-top:none'>
        <tr><td><label for="file"><span style='font-size:12px'><?php echo t("Video file") ?>:</span></label></td><td><input id="file" type="file" name="file" /></td></tr>
        <tr><td><label for="categoryid"><span style='font-size:12px'><?php echo t("Category") ?>:</span></label></td><td><select name="categoryid"><?php echo $options ?></select></td></tr>
        <tr><td><label for="title"><span style='font-size:12px'><?php echo t("Title") ?>:</span></label></td><td><input type="text" name="title" value="<?php echo $title ?>"/></td></tr>
        <tr><td><label for="description"><span style='font-size:12px'><?php echo t("Description") ?>:</span></label></td><td><textarea cols='30' rows='5' name='description' /></textarea></td></tr>
      </tbody>
    </table>
    <input type="hidden" name="success_url" value="<?php echo url('user/' . $user->uid . '/screen9', array('absolute' => TRUE)) ?>" />
    <input type="hidden" name="failure_url" value="<?php echo url('user/' . $user->uid . '/screen9', array('absolute' => TRUE)) ?>" />
    <input type="hidden" name="auth" value="<?php echo $uploadurl['auth'] ?>"/>
    <input type="hidden" id="uploadid" name="uploadid" value="<?php echo $uploadurl['mediaid'] ?>"/>
    </form>

    <p><input type="submit" name="upload" id="submit" alt="submit" value="<?php echo t("Upload")  ?>" /></p>
    <div id="upload_progress" class="progress ahah-progress ahah-progress-bar" style="display: none; ">
      <div class="bar">
        <div class="filled"></div>
      </div>
    </div>
  </body>
</html>
