<?php

/**
 * @file
 * upload_page.tpl.php
 *
 * Template for video upload popup.
 */

?>

<form accept-charset="UTF-8" method="post" id="screen9upload" enctype="multipart/form-data" action="<?php echo $uploadurl['url'] ?>?uploadid=<?php echo $uploadurl['mediaid'] ?>">
  <p><label for="file"><?php echo t("Video file") ?>:</label><br/><input id="file" type="file" name="file" /></p>
  <p><label for="categoryid"><?php echo t("Category") ?>:</label><br/><select name="categoryid"><?php echo $options ?></select></p>
  <p><label for="title"><?php echo t("Title") ?>:</label><br/><input type="text" name="title" value="<?php echo $title ?>"/></p>
  <p><label for="description"><?php echo t("Description") ?>:</label><br/><textarea cols='60' rows='5' name='description' /></textarea></p>

  <input type="hidden" name="success_url" value="<?php echo url('user/' . $user->uid . '/screen9', array('absolute' => TRUE)) ?>" />
  <input type="hidden" name="failure_url" value="<?php echo url('user/' . $user->uid . '/screen9', array('absolute' => TRUE)) ?>" />
  <input type="hidden" name="auth" value="<?php echo $uploadurl['auth'] ?>"/>
  <input type="hidden" id="uploadid" name="uploadid" value="<?php echo $uploadurl['mediaid'] ?>"/>
</form>

<p><input type="submit" name="upload" id="submit" alt="submit" value="<?php echo t("Upload") ?>" /></p>
<div id="upload_progress" class="progress ahah-progress ahah-progress-bar" style="display: none;">
  <div class="bar">
    <div class="filled" style="width: 0%;"></div>
  </div>
</div>
