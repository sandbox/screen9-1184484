var intervalId = "";
jQuery.extend
({
  'screen9_QueryString': window.location.search.length <= 1 ? new Array() :
  function(a) {
    var b = new Array();
    for (var i = 0; i < a.length; ++i) {
      var p=a[i].split('=');
      b[p[0]] = unescape(p[1]);
    }
    return b;
  }(window.location.search.substr(1).split('&'))
});

$(document).ready(function () {
  // Warning if leaving page before upload
  if ($("#screen9upload").length > 0) {
    window.onbeforeunload = function() { 
      return "Du håller på att lämna sidan utan att ditt bidrag har sparats."; 
    };
  }

  // when the submit button is clicked, disable the warning
  if ($("#screen9upload #submit").length > 0) {
    $("#submit").click(function() {
      alert('!');
      return false;

      window.onbeforeunload = null;
      $("#screen9upload").submit();
    });
  }

  $("#submit").click(function() {
    $("#screen9upload").submit();
  });

  if ($("#screen9upload").length > 0) {
    $("#screen9upload").submit(function() {
      if ($("input[name=title]").val() == '') {
        alert(Drupal.t('You must set a title.'));
        return false;
      }

      $("#submit").hide();
      $("#upload_progress").show();
      $('input').attr('readonly', 'readonly');

      intervalId = setInterval(function() {
        var uploadid = $("#uploadid").val();
        $.getJSON("?q=screen9/uploadprogress/"+uploadid, function(data) {
          if(data.data.upload_progress > 0) {
            $(".filled").width(Math.floor(data.data.upload_progress)+'%');
          }
        });
      }, 800);

      function validator(XMLHttpRequest, textStatus) {
        clearInterval(intervalId);
        $(".filled").width('100%');
        var url = $("iframe").contents()[0].URL;
        if (url.indexOf("message") != -1) {
          alert(unescape(url.substr(url.indexOf("message") + 8)));
        } else {
          var q1 = $.screen9_QueryString["returnmediaid"];
          var q2 = $.screen9_QueryString["elementid"];
          var q3 = $.screen9_QueryString["redir"];
          var q4 = $.screen9_QueryString["t"];
          if (q1 == 'true') {
            if(q3 == 'true'){
              if(q4 == ''){
                document.location.href = '/screen9/selectpopup';
                return false;
              } else {
                document.location.href = '/screen9/selectpopup?t='+q4;
                return false;
              }
            } else {
              var parent = $(window.opener.document);
              $(parent).find('#' + q2).val($("input[name=title]").val() + ' [' +  $("#uploadid").val() +']');
              window.close();
            }
          } else {
            document.location.href = url;
          }
        }
      }

      function error(XMLHttpRequest, textStatus, errorThrown){
      }

      function success(data, textStatus, XMLHttpRequest){
      }

      $("#screen9upload").ajaxSubmit({"complete": validator, "dataType": "text", "iframe": true, 'error':error,'success':success,'type':'POST'});
      return false;
    });
  }
});
