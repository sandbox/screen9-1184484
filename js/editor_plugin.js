(function() {
  tinymce.create('tinymce.plugins.s9media', {
    /**
    * Initializes the plugin, this will be executed after the plugin has been created.
    * This call is done before the editor instance has finished it's initialization so use the onInit event
    * of the editor instance to intercept that event.
    *
    * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
    * @param {string} url Absolute URL to where the plugin is located.
    */
    init : function(ed, url) {
      ed.addCommand('mces9media', function() {
        var theid = ed.selection.getNode().id;
        var validTarget = ed.selection.getContent().toString().search("s9_img_content");
        if(validTarget != -1){
          var editImgPopup = window.open('/screen9/editimage/'+theid, 'screen9', 'width=500, height=650, scrollbars=1');
          var timer2 = setInterval(function() {   
            if(editImgPopup.closed) {  
              clearInterval(timer2);  
              $.ajax({
                type: "POST",
                url: '/screen9/getimage',
                data: "i="+theid,
                dataType: "text",
                success: function(data){
                  var atts = data.split('data');
                  var align = 'align';
                  ed.dom.setAttrib(theid, 'src', atts[0]);
                  ed.dom.setAttrib(theid, 'width', atts[1]);
                  ed.dom.setAttrib(theid, 'height', atts[2]);
                  ed.dom.setAttrib(theid+'_container', 'width', atts[1]);
                  ed.dom.setAttrib(theid+'_container', 'height', atts[2]);
                  
                  if(atts[3] == 'top' || atts[3] == 'bot'){
                    align = 'valign'
                  }
                  
                  ed.dom.setAttrib(theid+'_container', align, atts[3]);
                }
              });
            }
          }, 2000);
        } else {
          var selectPopup = window.open('/screen9/selectpopup', 'screen9', 'width=700, height=600, scrollbars=1');
          var timer = setInterval(function() {   
            if(selectPopup.closed) {  
              clearInterval(timer);  
              var fullID = $('#s9_wysiwyg_holder').val();

              var mediaID = '';
              var components = '';
              var ascript = '';
              var hashes = '';
              var code = password(32);

              fullID.replace(/\[([^\]]+)\]/g, function() { 
                mediaID = arguments[1];
                return "";
              });
              // var rUrl = '/screen9/getv?vid='+mediaID+'&playerWidth='+playerWidth+'&playerHeight='+playerHeight+'&aspectRatio='+aspectRatio;
              var rUrl = '/screen9/getv?vid='+mediaID+'&code='+code;

              if(mediaID != ''){
                $.ajax({
                  type: "GET",
                  url: rUrl,
                  dataType: "text",
                  mimeType: "text/html",
                    beforeSend: function(xhr) {
                    xhr.setRequestHeader('Content-type','text/html');
                    },
                    success: function(data){
                      if(data == ''){
                        alert('The Screen9 API call failed. Please verify your Screen9 settings.');
                      } else {
                        verdata = data.split('data');
                        components = verdata[2].split('<script');
                        ascript = '<script'+components[1];
                        components[0] = components[0].replace('<img', '<img class="s9_img_content"');
                        tinyMCE.execCommand("mceInsertContent", false, '<div id="'+code+'_container">'+components[0]+'</div>');
                        var hashes = window.location.href.split('/');
                        hashes = hashes[4];

                        $.ajax({
                          type: "POST",
                          url: '/screen9/adds',
                          data: "w="+verdata[0]+"&h="+verdata[1]+"&o="+mediaID+"&n="+hashes+"&s="+ascript,
                          dataType: "text",
                          error: function(data){
                            alert('The Screen9 API call failed. Please verify your Screen9 settings.');
                          }
                        });
                      }
                    },
                    error: function(){
                      alert('The Screen9 API call failed. Please verify your Screen9 settings.');
                    }
                });
              }

              $('#s9_wysiwyg_holder').val('');
            }  
          }, 2000); 
        }
      });

      ed.addButton('s9media', {
        title : 'Screen9 Media',
        cmd : 'mces9media',
        image : url + '/../img/s9wysiwyg.png'
      });

      // Add a node change handler, selects the button in the UI when a image is selected
      ed.onNodeChange.add(function(ed, cm, n) {
        cm.setActive('s9media', n.nodeName == 'IMG');
      });
    },

    /**
    * Returns information about the plugin as a name/value array.
    * The current keys are longname, author, authorurl, infourl and version.
    *
    * @return {Object} Name/value array containing information about the plugin.
    */
    getInfo : function() {
      return {
        longname : 'Screen9 Media',
        author : 'The Farm',
        authorurl : 'http://thefarm.se',
        infourl : 'http://thefarm.se',
        version : "0.6"
      };
    }
  });

  // Register plugin
  tinymce.PluginManager.add('s9media', tinymce.plugins.s9media);
})();

function password(length, special) {
  var iteration = 0;
  var password = "";
  var randomNumber;
  if(special == undefined){
    var special = false;
  }
  while(iteration < length){
    randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
    if(!special){
      if ((randomNumber >=33) && (randomNumber <=47)) { continue; }
      if ((randomNumber >=58) && (randomNumber <=64)) { continue; }
      if ((randomNumber >=91) && (randomNumber <=96)) { continue; }
      if ((randomNumber >=123) && (randomNumber <=126)) { continue; }
    }
    iteration++;
    password += String.fromCharCode(randomNumber);
  }
  return password;
}
